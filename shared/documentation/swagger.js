const options = {
    swaggerDefinition: {
      info: {
        title: "Documentation API FindAChurch",
        version: "0.0.1",
        description:
          "API focused on serving data to the FindAChurd platform; platform which is responsible for recording data from Adventist churches and presenting them to the public.",
        license: {
          name: "MIT",
          url: "https://spdx.org/licenses/MIT.html",
        },
        contact: {
          name: "Daniel Valdez",
          url: "https://daniel-valdez.com",
          email: "hello@daniel-valdez.com",
        },
      },
      servers: [
        {
          url: "http://localhost:5000/api/",
          description: "Local Server"
        },
      ]
    },
    apis: ["documentation.js"]
  };

  module.exports = options;