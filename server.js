const express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    morgan = require('morgan'),
    swaggerJsdoc = require("swagger-jsdoc"),
    swaggerUi = require("swagger-ui-express");

const app = express();

app.use(morgan(':method :url :status :response-time ms'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use('/api', require('./shared/routes/index.routes'));

app.listen(5000, async() => {
    console.log('App run!');
});

const options = require('./shared/documentation/swagger');

const swaggerDoc = swaggerJsdoc(options);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));