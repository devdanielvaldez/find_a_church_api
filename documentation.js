/** 
 * @swagger
 * /api/welcome:
 *  post:
 *      summary: Login in the app
 *      consumes:
 *          - application/json
 *      parameter:
 *          - in: body
 *      name: Login
 *      description: Use to request all customer
 *      schema:
 *          type: object
 *      required:
 *          - username
 *          - password
 *      properties:
 *          username:
 *              type: string
 *          password:
 *              type: string
 *      responses:
 *          '200':
 *              description: A successful response
*/