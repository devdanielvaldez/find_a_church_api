const router = require('express').Router();
const authLogin = require('../controllers/auth/login.controller');

router.post('/welcome', authLogin.loginUser);

module.exports = router;